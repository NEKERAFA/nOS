<div style="text-align:center"><img src ="docs/logo.png"/></div>

[![Powered by Lua](https://img.shields.io/badge/lua-5.3-blue.svg)](https://www.lua.org/) [![license](https://img.shields.io/badge/license-GPL-blue.svg?logo=gnu&logoColor=white)](LICENSE)

nOS is a Open Source operative system simulator which works as a Lua framework application.
